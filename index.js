// require directive tells us to load the express module
const express = require("express");

// creating a server using express
const app = express();

// port
const port = 4000;

// middlewares
app.use(express.json());
// allows app to read a json data

app.use(express.urlencoded({ extended: true }));
// allows app to read data from forms
// by default, information received from the url can only be received as string or an array

let users = [
  {
    email: "nezukoKamado@gmail.com",
    username: "nezuko01",
    password: "let me out",
    isAdmin: false,
  },
  {
    email: "tanjiroKamado@gmail.com",
    username: "gonpanchiro",
    password: "iAmTanjiro",
    isAdmin: false,
  },
  {
    email: "zenitsuAgatsuma@gmail.com",
    username: "zenitsuSleeps",
    password: "iNeedNezuko",
    isAdmin: true,
  },
  {
    email: "hashibiraInosuke@gmail.com",
    username: "inosukeFight",
    password: "iNeedNezukoToo",
    isAdmin: false,
  },
  {
    email: "giyuTomioka@gmail.com",
    username: "giyuBestHashira",
    password: "iAmTooShy",
    isAdmin: false,
  },
];

let loggedUser;

app.get("/", (req, res) => {
  res.send("Hello World");
});

// app - server
// get - HTTP method
// '/' - routename or endpoint
// (req, res) -request and response - will handle the requests and the responses
// res.send - combines writeHead() and end(), used to send response to our client

// mini-activity
app.get("/hello", (req, res) => {
  res.send("Hello from Batch 131");
});

// POST
app.post("/", (req, res) => {
  console.log(req.body);
  res.send("Hello, I am POST method");
});

// mini-activity
app.post("/a", (req, res) => {
  console.log(req.body);
  res.send(
    `Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}`
  );
});

app.post("/users/register", (req, res) => {
  console.log(req.body);
  let newUser = {
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    isAdmin: req.body.isAdmin,
  };
  users.push(newUser);
  console.log(users);

  res.send(`User ${req.body.username} has successfully registered.`);
});

app.post("/users/login", (req, res) => {
  console.log(req.body);

  let foundUser = users.find((user) => {
    return (
      user.username === req.body.username && user.password === req.body.password
    );
  });
  if (foundUser !== undefined) {
    let foundUserIndex = users.findIndex((user) => {
      return user.username === foundUser.username;
    });
    foundUser.index = foundUserIndex;
    loggedUser = foundUser;
    console.log(loggedUser);
    res.send("You are now logged in.");
  } else {
    loggedUser = foundUser;
    res.send("Login Failed, wrong credentials.");
  }
});

// Change-Password Route

app.put("/users/change-password", (req, res) => {
  // store the message will be sent back to client
  let message;

  // will loop through all the "users" array
  for (let i = 0; i < users.length; i++) {
    if (req.body.username === users[i].username) {
      users[i].password = req.body.password;

      message = `User ${req.body.username}'s password has been changed.`;

      break;
    } else {
      message = `User not found.`;
    }
  }
  res.send(message);
  console.log(users);
});

// Activity

// 1.
app.get("/home", (req, res) => {
  res.send("Welcome");
});

// 3.
app.get("/users", (req, res) => {
  res.send(users);
});

// 5.
// app.delete("/delete-user", (req, res) => {
//   let message;

//   let foundUser = users.find((user) => {
//     return user.username === req.body.username;
//   });

//   let foundUserIndex = users.findIndex((user) => {
//     return user.username === foundUser.username;
//   });

//   users.splice(foundUserIndex, 1);
//   console.log(users);
//   return res.send("Success");
// });

app.delete("/delete-user", (req, res) => {
  // store the message will be sent back to client
  let message;

  // will loop through all the "users" array
  for (let i = 0; i < users.length; i++) {
    if (req.body.username === users[i].username) {
      let userIndex = users[i];
      users.splice(userIndex, 2);

      message = `User has been deleted.`;

      break;
    } else {
      message = `User not found.`;
    }
  }
  res.send(message);
  console.log(users);
});

// listen to the port and returning message in the terminal
app.listen(port, () => console.log(`Server is running at port ${port}`));
